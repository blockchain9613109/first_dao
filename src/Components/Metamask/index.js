import React, { Component } from 'react';
import { ethers } from "ethers";

import ERC20_ABI from "../../erc20_abi.json";

class Metamask extends Component {
    constructor(props) {
        super(props);

        this.state = {
        };
    }
    charlatansAdress = "0x16F778F4Fad736251B854611Bd6BA65c5616399E";

    async connectToMetamask() {
        const provider = new ethers.providers.Web3Provider(window["ethereum"])
        const accounts = await provider.send("eth_requestAccounts", []);
        const balance = await provider.getBalance(accounts[0]);
        const balanceInEther = ethers.utils.formatEther(balance);
        const block = await provider.getBlockNumber();

        provider.on("block", (block) => {
            this.setState({ block })
        })

        const charlatansContract = new ethers.Contract(this.charlatansAdress, ERC20_ABI, provider);
        const tokenName = await charlatansContract.name();
        const tokenBalance = await charlatansContract.balanceOf(accounts[0]);
        const tokenUnits = await charlatansContract.decimals();
        const tokenBalanceInEther = ethers.utils.formatUnits(tokenBalance, tokenUnits);

        this.setState({ selectedAddress: accounts[0], balance: balanceInEther, block, tokenName, tokenBalanceInEther})
    }

    async sendCharlatansTo() {
        const to = this.to.value;
        const amountInEther = this.amount.value;

        const provider = new ethers.providers.Web3Provider(window["ethereum"]);
        const signer = provider.getSigner()

        const charlatansContract = new ethers.Contract(this.charlatansAdress, ERC20_ABI, provider);

        const tokenUnits = await charlatansContract.decimals();
        const tokenAmountInEther = ethers.utils.parseUnits(amountInEther, tokenUnits);

        const charlatansContractWithSigner = charlatansContract.connect(signer);


        charlatansContractWithSigner.transfer(to, tokenAmountInEther);
    }

    renderMetamask() {
        if (!this.state.selectedAddress) {
            return (
                <button onClick={() => this.connectToMetamask()}>Connect to Metamask</button>
            )
        } else {
            return (
                <div>
                    <p>Welcome {this.state.selectedAddress}</p>
                    <p>Your MATIC Balance is: {this.state.balance}</p>
                    <p>Current ETH Block is: {this.state.block}</p>
                    <p>Balance of {this.state.tokenName} is: {this.state.tokenBalanceInEther}</p>
                    <div>
                        <input type="text" placeholder="Address" ref={(input) => this.to = input} />
                        <input type="number" placeholder="Amount" ref={(input) => this.amount = input} />
                        <button onClick={() => this.sendCharlatansTo()}>transfer</button>
                    </div>

                </div>
            );
        }
    }

    render() {
        return(
            <div>
                {this.renderMetamask()}
            </div>
        )
    }
}

export default Metamask;