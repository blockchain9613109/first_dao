import logo from './logo.svg';
import './App.css';
import Metamask from "./Components/Metamask";

function App() {
  return (
    <div className="App">
      <Metamask />
    </div>
  );
}

export default App;
